Stripe.setPublishableKey('pk_AeAXhPoWhC5PKt5PcFY7yGxyiqAsr');
$(function() {
  var $form = $('#donation-form');
  $form.submit(function(event) {
    // Disable the submit button to prevent repeated clicks:
    $form.find('#submit').prop('disabled', true);

    $form.find('#payment-message').text("Processing...");
    $("#payment-message").css({
      'opacity': '1',
      'max-height': '5em',
      'overflow': 'visible',
      'margin': '1em 0',
      'padding': '0.3em',
      'transform': 'scale(1)',
      'background-color': '#333'
    });

    // Request a token from Stripe:
    Stripe.card.createToken($form, stripeResponseHandler);
    // Prevent the form from being submitted:
    return false;
  });
});

function stripeResponseHandler(status, response) {
  // Grab the form:
  var $form = $('#donation-form');

  if (response.error) { // Problem!

    // Show the errors on the form:
    $form.find('#payment-message').text(response.error.message);

    $("#payment-message").css({'background-color': 'red'});

  } else { // Token was created!

    // Get the token ID:
    var token = response.id;

    // Insert the token ID into the form so it gets submitted to the server:
    $form.append($('<input type="hidden" name="stripe-token">').val(token));

    // Submit the form:
    $.ajax({
      type: "POST",
      url: "https://flask-rfahaiti.rhcloud.com/donation",
      dataType: 'html',
      data: $form.serialize(),
      crossDomain: true,
      success: function(data, textStatus) {
        $form.find('#payment-message').text("Payment succeeded.");
        $("#payment-message").css({'background-color': 'green'});
      },
      error: function(jqXHR, textStatus, errorThrown) {
        $form.find('#payment-message').text("There was a problem with payment. Your card was not charged. Please try again.");
        $("#payment-message").css({'background-color': 'red'});
      },
      complete: function() {
        $form[0].reset();
        $form.find('#submit').prop('disabled', false); // Re-enable submission
      }
    });
  }
};
