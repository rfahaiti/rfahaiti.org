---
title: Donate
permalink: /donate/
layout: page
---

If you would like to give to Restavec Freedom Alliance please click here. To learn more about our organization please visit our mission page for more information. We also have an essential items list which can be found here.

### Sponsoring a Child in our Program:

Several sponsorship opportunities are available for our children in Ducis, Haiti. Please go to the Ducis photo page here to see which children need to be sponsored.  To sponsor a child please click here.

You can also directly donate to Restavec Freedom Alliance, BEM Inc. by sending a check to:

Restavec Freedom Alliance, BEM Inc.
17 Windham Green Road
Windham, CT 06280 USA

**Restavec Freedom Alliance is a program of Bethesda Evangelical Mission – a nonprofit 501(c)3 organization in Haiti. Our EIN is: 31-1717364. 90% of your donation goes directly to our programs in Haiti.**
