---
title: History of RFA
permalink: /about/history/
layout: page
---

### History of Our Parent Organization:

Bethesda Evangelical Mission, has been involved in the lives of the Haitian people since 1984. They have a group of 35 churches all around the southern peninsula. BEM is dedicated to educating Haitian children through their 12 schools in the region, K-12. BEM run an orphanage through its division HELO, Inc., which provides home, education, love and opportunity for 42 children. Additionally, they have a micro loan program which is designed to aid Haitian entrepreneurs in the business economy. BEM is also digging water wells to help combat the problems of drinking unclean water. Ultimately, the holistic vision of B.E.M is to serve in response to the call of God: to teach, to restore health and promote freedom and well-being in the following ways: spiritually, physically, mentally/emotionally, socially and economically. To learn more about BEM, visit their website at: www.BemHaiti.org.

### History of Restavec Freedom Alliance:

The discussion surrounding the forming of the Restavec Freedom Alliance occurred in August 2010. During a mission visit working with the HELO orphanages, Pastor Jean Beaucejour spoke with Ellen, sharing with her the plight of the Restavec children and his vision. During deep discussions with her family upon returning to the states, Pastor Lubin in the US, and within her own soul, Ellen found that this was exactly what should be done and how she could best aid in helping. Working together with both Pastor Jean and Lubin, and others that spiritually supported her, the alliance became a reality and start-up money was given by the most generous of women. Fundraising officially began, and Pastor Jean set up a network within his communities to find which Restavec children he could free from their circumstances. The first area to house these children was determined, and the first parents were named. The first house was successfully opened in December of 2010 with our first group of children.  We then opened our second home in December of 2012. Currently we have a waiting list of another 100 children.
