---
title: About us
permalink: /about/
layout: page
---

*Restavec Freedom Alliance* is dedicated to providing relief for the [Restavec]({{ "/restavec" | prepend: site.baseurl }}) children, children who are “given” to families and used as slavery in Haiti. Our mission is to offer a safe, stable, family centered environment for these children so that each child can receive the childhood they deserve – nothing like their life as a Restavec. Each child will have access to an education, medical support, loving care, psychiatric care, and adequate food, clean water and sanitary facilities.
