---
title: Team
permalink: /about/team/
layout: page
---

Restavec Freedom Alliance has a diverse team of board member volunteers who all have one thing in common: they want to make a difference to help Haiti. Our current team is rather small though we plan to grow our board and staff as our organization grows.  If you believe you have skills and determination to help our organization please contact us.

### Ellen Graber Donohue
Ellen is the Executive Director of Restavec Freedom Alliance, BEM Inc. Ellen grew up in Wethersfield, CT and graduated from the University of Connecticut. She spent most of her career as an executive for an insurance company working in information technology. In her last role, Ellen spent two years in Tokyo heading a technology division. She retired from corporate life in 2010. Originally a Child Development major in college, Ellen returned to her roots working with poor children in the Dominican Republic starting in 2006. This work introduced her to Bethesda Evangelical Mission Inc, a church run non-profit organization working in Haiti. Ellen worked in orphanages there both before and after the earthquake. Most recently she took on the project of developing and managing a home and school for Restavec, or slave children in western Haiti. In addition, since retiring, Ellen has gone back to school and has obtained a Registered Nurse (RN) degree. She intends to use this training to further her work in Haiti.

### Patrick Nommensen
Patrick is the Assistant Director of Restavec Freedom Alliance, BEM Inc. Patrick lives in San Jose, California and attends Santa Clara University. His role in is to explore new ideas, advocate, and supply solutions in order for our organization to grow and ultimately allow more children to be in our programs.

### Elise Graber
Elise is the Director of Social Outreach. She grew up in a loving family, thinking that everyone had one. She was shocked to realize that wasn’t true. She believes that every child deserves to feel loved and believes that we can change the world, one child at a time, one smile at a time. Elise has been a teacher in middle school for 28 years, lives in Connecticut, loves teaching and can’t imagine doing anything else. She is married and has one son, Caleb, who is a chef and has also been to Haiti with Restavec Freedom Alliance. Elise doesn’t tolerate the Haitian heat very well, so her efforts are here in the states where she works endlessly to support this effort.

### Nina Brandi
Nina is the Co-Director of Social Outreach. It was fate that directed her to Haiti in May of 2012. The children have captured her heart and become part of her family. Nina lives in Hamden, Connecticut and attends Quinnipiac University. She is a Nursing Major with aspirations to become a pediatric nurse. Nina’s goal is to be able to extend her compassion for the children through not only the love in her heart, but on a medical level as well. Nina has been to Haiti two times, and plans to continue returning with hopes to make a difference in the lives of these beautiful children.

### Nancy Evans
Nancy is the Director of Marketing for RFA, primarily concentrating on website branding, organizational branding and the design of marketing materials. Nancy retired from the corporate workforce after working several years at DEC, delivering and installing software systems and services to the educational and government sectors. Having a long-standing passion for photography, she grew her portfolio while living in Clermont-Ferrand, France in the late 80s. 13 years ago, Nancy formed Auvergne Studios & Nancy Evans Photography, inspired by the richness of the landscape in central France.  Always involved in volunteerism, such as reading to the blind back at Syracuse University (’73 graduate), working at the Boston food bank, tutoring  Brazilian ESL students, Nancy became reacquainted with Ellen at Sturbridge Village, MA after a 30 hiatus! After Nancy learned about RFA/Haiti from Ellen, she was hooked.  She is now planning her 3rd trip to Haiti in January 2013.
