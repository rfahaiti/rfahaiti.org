---
title: What are Restavec?
permalink: /restavec/
layout: page
---

Haiti has a large number of children who live outside their parents homes, and serve in deplorable situations. These children are known as Restavec.

A report from the United Nations states that “Restavec” in Creole means “staying with” and is a term used for children who are sent to live with other families as slaves.  The move often lands Restavec in a situation of unpaid domestic service where they are deprived of their most basic right.

### In pre-earthquake Haiti there were about 250,000 Restavec in Haiti, two-thirds of which are girls.

Due to the crushing poverty rate, many poor parents believe the city offers great opportunity and better educational programs for their children. However, that is usually never the case. The young children are offered as servants to families, distant relatives, friends or total strangers. Often, an education or trade is promised for the child. This promise is false.  The average child works from daybreak to sundown on household chores. They have don’t have a childhood, do not receive proper care and have no future. Restavec children are not allowed to go to school – they have no education.

### Restavec are often beaten and abused sexually. They are *powerless*.

The Restavec system in Haiti has been declared by [Gulnara Shahinian](https://en.wikipedia.org/wiki/United_Nations_Human_Rights_Council), the UN’s Special Raporteur on Contemporary Forms of Slavery, as tantamount to slavery. Any form of modern slavery is a human rights violation anywhere in the world, according to [Article 4 of The Universal Declaration of Human Rights](https://www.un.org/en/documents/udhr/index.shtml) adopted by the United Nations General Assembly in 1948, which states, “No one shall be held in slavery or servitude; slavery and the slave trade shall be prohibited in all their forms.”

The Haitian government has acknowledged that the Restavec system is a form of modern child slavery, and has made the ownership of Restavecs illegal, though has taken no actions to stop the practice of it, and has even remarked that it is part of the Haitian culture.

If you would like to learn more about Restavec, please watch the two videos below. If you have any further questions, we would love to hear from our supporters.  Please feel free to [contact]({{ "/contact" | prepend: site.baseurl }}) us.
