---
title: Volunteer in Haiti with RFA
permalink: /volunteer/
layout: page
---

Would you like to make a difference and help Haiti? Volunteer in Haiti! If you are interested in volunteering with our program please submit an application here.

**Quinnipiac University Trip**: March 8 – 15, 2014

**Summer Trip**: July 14 - 21, 2014

*Note, we are planning to schedule a trip end of May or early June as well as end of August. If you are interested to volunteer during this time frame please contact us.*

**We have a need for all kinds of skill-sets. Here are a few:**

- **Construction** - We will be planning and building homes and a school on our land in Chantal. A new piece of land has been purchased and is ready to be built on. We will be building a new home and a school for the community – one of the best ways we can help Haiti.
- **Medical** - Bethesda Evangelic Mission runs medical clinics, we have teams that go every quarter. If you are a health care provider we would love to have you join us.
- **Dental** - We are looking to have our children reviewed by a dentist and have any work required done.
- **Therapy** - We plan on creating and presenting programs in Art Therapy, and Play Therapy.
- **Literacy** - We plan on putting together literacy programs for the community.
- **Education** - Along with the completion of our new house, we plan to build a school for the community. If you are a teacher or other education provider we need your help setting up a new school.
- **Ecology** - We plan on having programs to teach the parents, children and community about how to take care of the beautiful land.
- **Farming** - We plan to farm the land and grow food for our children to eat, as well as to share with the community.
- **Sports and Play** - We plan on building a play yard for the children, and helping them learn how to play team sports.

We are glad that you are interested in joining us on a trip to volunteer Haiti. If you would like to volunteer please fill out the application here.  The total cost for our trips is $1,600. Please note that the $800 down payment for the commercial airline ticket is non-refundable by RFA – it is your responsibility to work with the airline, and we will supply all necessary information for you. You can submit payment here.
